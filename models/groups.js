var defineMethods = require("./defineMethods.js");

module.exports = function(pgClient){

   return defineMethods(pgClient, {
      list : "\
         SELECT \
            * \
         FROM groups",
      set : "\
         INSERT INTO \
            groups \
            (\
               name \
            )\
         VALUES \
            (\
               $name \
            );",
      delete : "\
         DELETE \
         FROM \
            groups \
         WHERE \
            id = $id;"
   });
};