var defineMethods = require("./defineMethods.js");

module.exports = function(pgClient){

   return defineMethods(pgClient, {
      list : "\
         SELECT \
            id, (contacts.firstname || ' ' || contacts.secondname) as fio, phone\
         FROM contacts \
         [WHERE \
            groupid = $groupId]",
      set : "\
         INSERT INTO \
            contacts \
            (\
               firstname, \
               secondname, \
               phone,\
               email,\
               groupid\
            )\
         VALUES \
            (\
               $firstname, \
               $secondname, \
               $phone, \
               $email, \
               $groupid \
            );",
      get : "\
         SELECT \
            * \
         FROM \
            contacts \
         WHERE \
            id = $id;",
      update : "\
         UPDATE \
            contacts \
         SET \
            firstname  = $firstname, \
            secondname = $secondname, \
            phone      = $phone, \
            email      = $email, \
            groupid    = $groupid \
         WHERE \
            id = $id;",
      delete : "\
         DELETE \
         FROM \
            contacts \
         WHERE \
            id = $id;",
      removeGroup : "\
         DELETE \
         FROM \
            contacts \
         WHERE \
            groupid = $id;"
   });
};