module.exports = function(pqClient, methods){
   var m = {};

   for (var i in methods){
      if (methods.hasOwnProperty(i)){

         if (typeof methods[i] === "function"){
            m[i] = methods[i];
         }
         else{
            var query, cb;

            if (typeof methods[i] === "string"){
               query = methods[i];
            }
            else if (typeof methods[i] === "object"){
               query = methods[i]["query"];
               cb = methods[i]["callback"];
            }

            m[i] = (function(q, cb){
               return function(fields, callback){
                  var query = q;
                  /**
                   * обрабатываем конструкции типа SELECT * WHERE a = $a[ AND b = $b]
                   * выражение заключенное в скобки является опциональным,
                   * т.е если не найдена хотя бы одна указанная переменная, конструкция исключается из запроса
                   */
                  query = query.replace(/\[[^\]]+\]/g, function(str){
                     var
                        cleanStr = false,
                        results = str.match(/\$\w+|\{\$\w+\}/g);

                     str = str.replace(/\[|\]/g,"");

                     for (var i = 0, l = results.length; i < l; i++){
                        var v = results[i].replace(/\$|\{|\}/g, "");
                        if (fields[v] === undefined){
                           cleanStr = true;
                           break;
                        }
                     }

                     if (cleanStr)
                        str = "";

                     return str;
                  });

                  /**
                   * инлайним переменные $var или {$var}
                   */
                  query = query.replace(/\$(\w+)|\{\$(\w+)\}/g, function(str, v1, v2){
                     var val = fields[v2 ? v2 : v1];
                     if (val === undefined){
                        val = "null";
                     }
                     else if (typeof val == "string"){
                        val = ["'", val.replace(/'/g,"\""),"'"].join("");
                     }
                     return val;
                  });
                  pqClient.query(query, function(err, res){
                     if (!err){
                        var rows = "rows" in res ? res["rows"] : [];
                        if (typeof cb === "function"){
                           cb(null, rows, callback)
                        }
                        else{
                           if (typeof callback === "function")
                              callback(null, rows)
                        }
                     }
                     else{
                        console.log("Ошибка при работе с БД\n"+ err.message +"\nQuery :\n" + query);
                        if (typeof cb === "function"){
                           cb(err, undefined, callback);
                        }
                        else{
                           if (typeof callback === "function")
                              callback(err);
                        }
                     }
                  });
               }
            })(query, cb);
         }
      }
   }

   return m;
};