define(
   "js!core.Search",
   ["js!CompoundControl", "js!EventBus", "html!core.Search", "css!core.Search", "js!core.Button", "js!core.String"],
   function(CompoundControl, EventBus, dotTplFn){

      var Search = CompoundControl.extend({
         $protected : {
            _dotTplFn  : dotTplFn,
            _options : {
            }
         },
         $constructor : function(cfg){
            Search.super.call(this, cfg);

            this._publish("onSearch");

            var self = this;

            this._btn = this._components["button"];
            this._str = this._components["string"];

            this._btn.click(function(){
               self._notify("onSearch", self._str.val());
            });
         },
         onSearch : function(fn){
            this.subscribe("onSearch", fn)
         }
      });

      return Search;
   }
);
