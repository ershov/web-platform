define("js!core.Button", ["js!BaseControl", "html!core.Button", "css!core.Button"], function(BaseControl, dotTplFn){

   var Button = BaseControl.extend({
      $protected : {
         _dotTplFn  : dotTplFn,
         _container : null,
         _components: null,

         _options : {
            caption : "caption"
         }
      },
      $constructor : function(cfg){
         Button.super.call(this, cfg);
      },
      click  : function(fn){
         $(this._container).click(fn)
      }
   });

   return Button;
});