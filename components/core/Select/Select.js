define("js!core.Select", ["js!DataBoundControl", "html!core.Select", "css!core.Select"], function(DataBoundControl, dotTplFn){

   var Select = DataBoundControl.extend({
      $protected : {
         _dotTplFn  : dotTplFn,
         _options : {
            data : []
         }
      },
      $constructor : function(cfg){
         Select.super.call(this, cfg);
         var self = this;
         $(this._container).change(function(e){
            self._updateContextValue(self._getValue());
         })
      },
      setData : function(data){
         this._options.data = data;
         this._redraw();
         this.val(this._options.value);
      },
      _redraw : function(){
         var incubator = document.createElement("select");

         this._container.innerHTML = "";

         incubator.innerHTML = this._dotTplFn(this._options);
         incubator = incubator.childNodes[0];

         while(incubator.childNodes.length){
            this._container.appendChild(incubator.childNodes[0]);
         }
      },
      _setValue : function(val){
         $(this._container).val(val);
      },
      _getValue : function(){
         return $(this._container).val();
      }
   });

   return Select;
});