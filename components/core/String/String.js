define("js!core.String", ["js!DataBoundControl", "html!core.String", "css!core.String"], function(DataBoundControl, dotTplFn){

   var String = DataBoundControl.extend({
      $protected : {
         _dotTplFn  : dotTplFn,
         _container : null,
         _components: null
      },
      $constructor : function(cfg){
         String.super.call(this, cfg);
         var self = this;
         $(this._container).bind("keyup change", function(e){
            self._updateContextValue(self._getValue());
         })
      },
      _setValue : function(val){
         $(this._container).val(val);
      },
      _getValue : function(){
         return $(this._container).val();
      }
   });

   return String;
});