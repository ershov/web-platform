define(
   "js!contacts.List",
   ["js!BaseControl", "html!contacts.List", "js!HashManager", "css!contacts.List"], function(BaseControl, dotTplFn, HashManager){

   var ContactsList = BaseControl.extend({
      $protected : {
         _dotTplFn  : dotTplFn,
         _options : {
            firstRequest : false,
            filter : {},
            list : []
         }
      },
      $constructor : function(cfg){
         ContactsList.super.call(this, cfg);

         HashManager.on("group", this._hashHandler, this);
         this._hashHandler(HashManager.get())
      },
      reload : function (){
         var self = this;
         var filters = this._prepareFilters();
         $.getJSON("/data/list?" + filters, function(list, type){
            if (type == "success"){
               self._options.list = list;
               self._redraw();
            }
         })
      },
      _redraw : function(){
         var incubator = document.createElement("div");

         this._container.innerHTML = "";

         incubator.innerHTML = this._dotTplFn(this._options);
         incubator = incubator.childNodes[0];

         while(incubator.childNodes.length){
            this._container.appendChild(incubator.childNodes[0]);
         }
      },
      _prepareFilters : function(){
         var m = [];
         for (var i in this._filter){
            if (this._filter.hasOwnProperty(i)){
               m.push([i, this._filter[i]].join("="));
            }
         }
         return m.join("&");
      },
      _hashHandler : function(data){
         var id = data["target"];
         if (id && id !== "all"){
            this._filter = {groupId : id};
         }
         else{
            this._filter = {};
         }

         this.reload();
      }
   });

   return ContactsList;
});