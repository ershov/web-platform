define(
   "js!contacts.Groups",
   ["js!BaseControl", "js!HashManager", "js!EventBus", "html!contacts.Groups", "css!contacts.Groups"],
   function(BaseControl, HashManager, EventBus, dotTplFn){

      var ContactsGroups = BaseControl.extend({
         $protected : {
            _dotTplFn  : dotTplFn,
            _container : null,
            _components: null,

            _options : {
               list : []
            }
         },
         $constructor : function(cfg){
            ContactsGroups.super.call(this, cfg);
            var self = this;
            this.reload();

            HashManager.on("group", this._activateRow, this);

            $(this._container).on("click", ".ws-ContactsGroups__delete", function(){
               var id = $(this).parents(".ws-ContactsGroups__item").data("id");
               $.ajax("/data/groupRemove", {
                  data : {id : id},
                  success : function(res){
                     if (res.result == "success"){
                        self.reload();
                     }
                     else{
                        alert("Ошибка");
                     }
                  }
               });
               return false;
            });

            EventBus.channel("contacts").on("groupsChanged", function(){
               this.reload();
            }, this);
         },
         reload : function (){
            var self = this;
            $.getJSON("/data/groupList", function(list, type){
               if (type == "success"){
                  self._options.list = list;
                  self._redraw();
               }
            })
         },
         _redraw : function(){
            var incubator = document.createElement("div");

            this._container.innerHTML = "";

            incubator.innerHTML = this._dotTplFn(this._options);
            incubator = incubator.childNodes[0];

            while(incubator.childNodes.length){
               this._container.appendChild(incubator.childNodes[0]);
            }
            this._activateRow(HashManager.get());
         },
         _activateRow : function(data){
            var id = data["target"] || "all";
            $(this._container).find(".ws-ContactsGroups__item").removeClass("ws-ContactsGroups__item__active-true");
            $(this._container).find(".ws-ContactsGroups__item[data-id='" + id + "']").addClass("ws-ContactsGroups__item__active-true");
         }
      });

      return ContactsGroups;
   }
);