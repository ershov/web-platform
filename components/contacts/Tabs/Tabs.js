define("js!contacts.Tabs", ["js!CompoundControl", "html!contacts.Tabs", "css!contacts.Tabs"], function (CompoundControl, dotTplFn) {

   var Tabs = CompoundControl.extend({
      $protected : {
         _dotTplFn  : dotTplFn,
         _options : {
            tabs : []
         }
      },
      $constructor : function(cfg){
         Tabs.super.call(this, cfg);

         this._initBehavior();
      },
      _initBehavior : function(){
         var container = $(this._container);

         container.on("click", ".contacts-Tabs__btn", function(){
            var
               clicked = $(this),
               viewId  = clicked.data("for"),
               btns    = container.find(".contacts-Tabs__btn"),
               views   = container.find(".contacts-Tabs__view");

            views.removeClass("contacts-Tabs__view__active-true");
            views.filter("[data-tab='"+ viewId +"']").addClass("contacts-Tabs__view__active-true");
            btns.removeClass("contacts-Tabs__btn__active-true");
            clicked.addClass("contacts-Tabs__btn__active-true");
         });
      }
   });

   return Tabs;

});