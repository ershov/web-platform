define(
   "js!contacts.EditContact",
   ["js!CompoundControl", "html!contacts.EditContact", "css!contacts.EditContact", "js!core.Button", "js!core.String", "js!core.Select", "js!contacts.Tabs"],
   function(CompoundControl, dotTplFn){

      var ContactsEditContact = CompoundControl.extend({
         $protected : {
            _dotTplFn  : dotTplFn,
            _options : {
               data : {}
            }
         },
         $constructor : function(cfg){
            ContactsEditContact.super.call(this, cfg);

            var self = this;

            this.getContext().setContextData(this._options.data);

            $.getJSON("/data/groupList", function(list, type){
               if (type == "success"){
                  self._components["groupid"].setData(list);
               }
            });

            this._components["save"].click(function(){
               $.ajax("/data/save", {
                  data : self.getContext().toObject(),
                  success : function(res){
                     if (res.result == "success"){
                        alert("Сохранено");
                     }
                     else{
                        alert("Ошибка");
                     }
                  }
               })
            });

            this._components["delete"].click(function(){
               $.ajax("/data/remove", {
                  data : self.getContext().toObject(),
                  success : function(res){
                     if (res.result == "success"){
                        window.location = "/";
                     }
                     else{
                        alert("Ошибка");
                     }
                  }
               })
            })
         }
      });

      return ContactsEditContact;
   }
);
