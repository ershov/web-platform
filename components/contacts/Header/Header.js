define(
   "js!contacts.Header",
   ["js!CompoundControl", "js!EventBus", "html!contacts.Header", "css!contacts.Header", "js!core.Search"], function(CompoundControl, EventBus, dotTplFn){

   var ContactsHeader = CompoundControl.extend({
      $protected : {
         _dotTplFn  : dotTplFn,
         _container : null,
         _options : {}
      },
      $constructor : function(cfg){
         ContactsHeader.super.call(this, cfg);

         this._components["addContact"].click(function(){
            window.location = "/index/edit/";
         });

         this._components["addGroup"].click(function(){
            $.ajax("/data/addGroup", {
               data : {"name" : window.prompt("Название группы")},
               success : function(res){
                  if (res.result == "success"){
                     EventBus.channel("contacts").trigger("groupsChanged")
                  }
                  else{
                     alert("Ошибка");
                  }
               }
            });
         })
      }
   });

   return ContactsHeader;
});