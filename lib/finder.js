var fs = require('fs');
var nodePath = require('path');
var doT = require("dot");
var htmlTpl = doT.template('define("html!{{=it.name}}", function(){return {{=it.fn}}})');
var cash = {};


function find(name, ext, cb, p){
   var
      paths = p || [
         nodePath.join(process.domain["core"], "lib"),
         process.domain["components"],
         process.domain["modules"]
      ],
      dir = paths.shift(),
      first = nodePath.join(dir, name + "." + ext),
      second = nodePath.join(dir, name, name.split("/").pop() + "." + ext);

   fs.exists(first, function(exists){
      if (exists){
         fs.readFile(first, "utf8", cb);
      }
      else{
         fs.exists(second, function(exists){
            if (exists){
               fs.readFile(second, "utf8", cb);
            }
            else{
               if (paths.length){
                  find(name, ext, cb, paths);
               }
               else{
                  cb(new Error("Модуль не найден!"));
               }
            }
         });
      }
   });
}



module.exports = {
   get : function(path, ext, cb){
      var cacheKey = [ext, path].join("!");
      if (cacheKey in cash){
         cb(null, cash[cacheKey]);
      }
      else{
         find(path, function(err, file){
            if (!err){
               if (ext == "html"){
                  try{
                     file = htmlTpl({
                        name : path,
                        fn : doT.template(file)
                     })
                  }
                  catch (e){
                     cb(err);
                  }
               }
               cash[path] = file;
            }
            cb(err, file);
         });
      }
   }
};