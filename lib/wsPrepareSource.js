var fs = require("fs");
var less = require("less");
var dot = require("dot");
var nodePath = require("path");
var htmlTpl = dot.template('define("html!{{=it.name}}", function(){return {{=it.fn}}})');
var tmp = {};
var getName = function(path){
   var
      namespace = /components\/([^\/]*)/g.exec(path)[1],
      name = /[^\/]*\..*$/g.exec(path)[0],
      p = /[^\/]+(?=\/[^\/]+\..*$)/g.exec(path)[0],
      result = "";

   result += namespace + ".";
   if (name.replace(/\..*$/, "") == p){
      result += p;
   }
   else{
      result += path.replace(new RegExp("components\\/"+ namespace +"\\/|\\..*$", "g"), "")
   }

   return result;
};

var techMap = {
   ".css" : {
      ext : ".less",
      prepare : function(path, file, cb){
         less.render(file, function (e, css) {
            if (e){
               cb(e)
            }
            else{
               cb(null, css, "text/css");
            }
         });
      }
   },
   ".dot.js" : {
      ext : ".xhtml",
      prepare : function(path, file, cb){
         try {
            try{
               var r = htmlTpl({
                  name : getName(path),
                  fn : dot.template(file)
               });
               cb(null, r, "text/javascript");
            }
            catch (e){
               cb(e);
            }
         }
         catch (e){
            cb(e);
         }
      }
   }
};

module.exports = function(path, cb){
   var exts = path.match(/\.[^\/]*$/);
   if (exts.length && exts[0] in techMap){
      var
         tech = techMap[exts[0]],
         p = nodePath.join(process["domain"]["rootPath"], path.replace(/\.[^\/]*$/, tech["ext"]));

      fs.readFile(p, "utf8", function(err, file){
         if (err){
            cb(err);
         }
         else{
            tech["prepare"](path, file, function(err, res, contentType){
               if (err){
                  cb(err);
               }
               else{
                  cb(null, tmp[path] = res, contentType);

                  //TODO : включить создание файла непосредственно на диске, для режима production
                  /*fs.writeFile(nodePath.join(process["domain"]["rootPath"], path), res, function(err){
                     if (!err){
                        delete tmp[path];
                     }
                  });*/
               }
            });
         }
      })
   }
};
