var Component = function(config){
   this.config = config;
};

Component.prototype.toString = function(){
   return "<ws:"+ this.config.className +" config='"+ JSON.stringify(this.config.options) +"'/>";
};

Component.prototype.getName = function(){
   return this.config.className;
};

module.exports = Component;