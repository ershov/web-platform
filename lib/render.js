var fs = require("fs");
var Component = require("./Component.js");
var doT = require("dot");

module.exports = function(viewPath, options, fn){

   var dependencies = [];

   for (var i in options){
      if (options.hasOwnProperty(i)){
         if (options[i] instanceof Component){
            dependencies.push("js!" + options[i].getName());
         }
      }
   }
   options["dependencies"] = JSON.stringify(dependencies);
   options["clientControllers"] = JSON.stringify(options["clientControllers"] || []);

   fs.readFile(viewPath, "utf8", function(err, file){
      if (!err){
         fn(null, doT.template(file)(options));
      }
      else{
         fn(err);
      }
   });
};