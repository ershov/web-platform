var ArgumentParser = require('argparse').ArgumentParser;
var domain = require('domain').create();
var express = require('express');
var app = express();
var fs = require('fs');
var nodePath = require('path');
var doT = require("dot");
var wsPrepareSource = require("./lib/wsPrepareSource.js");



/////////////////////////////
var pg = require('pg');
var conString = "postgres://postgres:postgres@localhost:5432/contacts";
var pgClient = new pg.Client(conString);

pgClient.connect();

global.pgClient = pgClient;
/////////////////////////////


var parser = new ArgumentParser({
   version: require("./package.json")["version"],
   addHelp: true,
   description: 'WI.SBIS static preprocessor'
});

parser.addArgument(
   [ '--config' ], {
      help: 'Config file location',
      defaultValue: nodePath.join(__dirname, '/config.json')
   });

var parsedArgs = parser.parseArgs();
var config = require(parsedArgs["config"]);
var paths = config["paths"];


//static files
app.use('/core', express.static(paths["core"]));
app.use('/components', express.static(paths["components"]));
app.use('/views', express.static(paths["views"]));

app.get(/\/components\/.*/,function(req, res, next){
   wsPrepareSource(req.path, function(err, file, contentType){
      if (err){
         next();
      }
      else{
         res.type(contentType);
         res.send(file);
      }
   })
});

app.set('view engine', 'xhtml');
app.engine('xhtml', require('./lib/render'));

domain.on("error", function(err){
   throw err;
});

domain.run(function(){
   //routing
   app.all(/\/(?:([^\/]*)\/?)?(?:([^\/]*)\/?)?(.*)?/, require('./lib/router'));

   process.domain["express"]    = app;
   process.domain["rootPath"]   = __dirname;
   process.domain["components"] = nodePath.resolve(__dirname, paths["components"]);
   process.domain["modules"]    = nodePath.resolve(__dirname, paths["modules"]);
   process.domain["views"]      = nodePath.resolve(__dirname, paths["views"]);
   process.domain["core"]       = nodePath.resolve(__dirname, paths["core"]);

   app.listen(process.env.PORT || 555);
   console.log("application was started");
});
