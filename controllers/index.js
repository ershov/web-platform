var path = require("path");
var Component = require(path.join("../lib", "Component.js"));
var contacts = require("../models/contacts.js")(global.pgClient);

module.exports = {
   index : function(req, res){
      res.render("doubleColumn", {
         title : "Телефонный справочник",
         header: new Component({
            className : "contacts.Header",
            options : {
               name : "ContactsHeader"
            }
         }),
         left : new Component({
            className : "contacts.Groups",
            options : {
               name : "ContactsGroups",
               list : []
            }
         }),
         right : new Component({
            className : "contacts.List",
            options : {
               name : "ContactsList",
               list : []
            }
         })
      }, function(err, file){
         if (!err){
            res.set('Content-Type', 'application/xhtml+xml');
            res.send(file);
         }
         else{
            throw err;
         }
      });
   },
   edit : function(req, res){
      contacts.get({"id" : req.query["id"]}, function(err, result){
         var data;
         if (!err){
            data = result[0];
         }

         res.render("main", {
            content : new Component({
               className : "contacts.EditContact",
               options : {
                  name : "ContactsEditContact",
                  data : data
               }
            })
         }, function(err, file){
            if (!err){
               res.set('Content-Type', 'application/xhtml+xml');
               res.send(file);
            }
            else{
               throw err;
            }
         });
      });

   }
};
