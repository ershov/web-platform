var contacts = require("../models/contacts.js")(global.pgClient);
var groups = require("../models/groups.js")(global.pgClient);
module.exports = {
   list : function(req, res){
      contacts.list(req.query, function(err, result){
         if (err){
            res.send(403);
         }
         else{
            res.json(result);
         }
      });
   },
   save : function(req, res){
      function sendRes(err){
         res.json({result : err ? "error" : "success"});
      }
      contacts.get(req.query, function(err, result){
         if (!err){
            if (result.length){
               contacts.update(req.query, sendRes);
            }
            else{
               contacts.set(req.query, sendRes);
            }
         }
         else{
            sendRes(err);
         }
      });
   },
   remove : function(req, res){
      contacts.delete(req.query, function(err){
         res.json({result : err ? "error" : "success"});
      })
   },
   addGroup : function(req, res){
      groups.set(req.query, function(err){
         res.json({result : err ? "error" : "success"});
      })
   },
   groupList : function(req, res){
      groups.list({}, function(err, result){
         if (err){
            res.send(403);
         }
         else{
            res.json(result);
         }
      });
   },
   groupRemove : function(req, res){
      contacts.removeGroup(req.query, function(err){
         if (!err){
            groups.delete(req.query, function(err){
               res.json({result : err ? "error" : "success"});
            })
         }
         else{
            res.json({result : "error"});
         }

      })

   }
};
