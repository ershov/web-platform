requirejs.config({
   baseUrl: '/core/',
   paths : {
      //Описываем расположение плагинов для requirejs
      "ws-path-resolver" : 'ext/requirejs/plugins/ws-path-resolver',
      "html"     : 'ext/requirejs/plugins/html',
      "css"      : 'ext/requirejs/plugins/css',
      "js"       : 'ext/requirejs/plugins/js',
      "r-css"        : 'ext/requirejs/plugins/r-css',
      "css-normalize": 'ext/requirejs/plugins/css-normalize',
      "text"         : 'ext/requirejs/plugins/text',
      "jQuery"       : 'ext/jquery/jquery-1.10.1',
      "doT"          : 'ext/doT/doT'
   }
});