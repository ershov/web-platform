define("core", ["jQuery", "js!ioc"], function($, ioc){
   var core;

   core = {
      ioc : new ioc(),
      /**
       * Генерация хэша переданных параметров/объекта.
       */
      hash : function(){
         var
            src = arguments.length > 1 ? Array.prototype.slice.call(arguments) : arguments[0],
            dst = {};
         if (src instanceof Array || src instanceof Object){
            for (var i in src){
               if (src.hasOwnProperty(i)){
                  if (typeof src[i] in {"boolean":null, "number":null, "string":null})
                     dst[src[i].toString()] = i;
               }
            }
         }
         return dst;
      },
      /**
       * Распаковка абстрактного набора аргументов в массив скаляров.
       */
      unpack : function(hash/*, unpackTypes*/){
         var
            args = [hash],
            unpackTypes = core.hash(arguments[1] !== undefined ? arguments[1] : ["array", "object", "function"]),
            ready;
         do{
            var result = [];
            ready = true;
            for (var i = 0, li = args.length; i < li; i++){
               var
                  arg = args[i],
                  j, lj;
               ready = false;
               if (arg instanceof Function && "function" in unpackTypes)
                  result.push(arg());
               else if (arg instanceof Array && "array" in unpackTypes){
                  for (j = 0,lj = arg.length; j < lj; j++)
                     result.push(arg[j]);
               }
               else if (arg instanceof Object && !(arg instanceof Function) && !(arg instanceof Array) && "object" in unpackTypes){
                  for (j in arg){
                     if (arg.hasOwnProperty(j))
                     result.push(arg[j]);
                  }
               }
               else{
                  ready = true;
                  result.push(arg);
               }
            }
            args = result;
         }
         while (!ready);
         return args;
      },
      /**
       * Объединение двух hash.
       * @param {Object} hash Исходный хэш
       * @param {Object} hashExtender хэш-расширение
       * @param {Object} [cfg] Параметры работы
       * <pre>
       *    preferSource: false // не сохранять исходное значение
       *    rec: true // объединять рекурсивно
       *    clone: false // клонировать пустые элементы
       *    create: true // создавать элементы которых нет в исходном хэше
       *    noOverrideByNull: false // не заменять значения null'овыми
       * </pre>
       */
      merge : function(hash, hashExtender, cfg){
         if (cfg === undefined)
            cfg = {};
         cfg.preferSource = cfg.preferSource !== undefined ? cfg.preferSource : false; // не сохранять исходное значение
         cfg.rec = cfg.rec !== undefined ? cfg.rec : true;  // объединять рекурсивно
         cfg.clone = cfg.clone !== undefined ? cfg.clone : false; // клонировать пустые элементы
         cfg.create = cfg.create !== undefined ? cfg.create : true; // создавать элементы которых нет в исходном хэше
         cfg.noOverrideByNull = cfg.noOverrideByNull  !== undefined ? cfg.noOverrideByNull : false; // не заменять значения null'овыми

         var cloneOrCopy = function(i){
            /**
             * Если к нам пришел объект и можно клонировать
             * Запускаем мерж того что пришло с пустым объектом (клонируем ссылочные типы)
             */
            if ((typeof(hashExtender[i]) == 'object' && hashExtender[i] !== null) && cfg.clone)
               hash[i] = core.merge(hashExtender[i] instanceof Array ? [] : {}, hashExtender[i], cfg);
            /**
             * Иначе (т.е. это
             *  ... не объект (простой тип) или
             *  ... запрещено клонирование)
             *
             * Если к нам пришел null и запрещено им заменять - не копируем.
             */
            else if(!(hashExtender[i] === null && cfg.noOverrideByNull))
               hash[i] = hashExtender[i];
         };

         if(typeof(hash) == 'object' && hash !== null && typeof(hashExtender) == "object" && hashExtender !== null){
            for (var i in hashExtender){
               if (hashExtender.hasOwnProperty(i)){
                  // Если индекса в исходном хэше нет и можно создавать
                  if (hash[i] === undefined) {
                     if(cfg.create) {
                        if(hashExtender[i] === null)
                           hash[i] = null;
                        else
                           cloneOrCopy(i);
                     }
                  }
                  else if (!cfg.preferSource){ // Индекс есть, исходное значение можно перебивать
                     if (hash[i] && typeof hash[i] == "object" && typeof hashExtender[i] == "object") {
                        // Объект в объект
                        if(hash[i] instanceof Date) {
                           if(hashExtender[i] instanceof Date) {
                              hash[i] = cfg.clone ? new Date(+hashExtender[i]) : hashExtender[i];
                              continue;
                           }
                           hash[i] = hashExtender[i] instanceof Array ? [] : {};
                        } else {
                           if(hashExtender[i] instanceof Date) {
                              hash[i] = cfg.clone ? new Date(+hashExtender[i]) : hashExtender[i];
                              continue;
                           }
                        }
                        hash[i] = cfg.rec ? core.merge(hash[i], hashExtender[i], cfg) : hashExtender[i];
                     }
                     else // Перебиваем что-то в что-то другое...
                        cloneOrCopy(i);
                  }
                  /**
                   * Исходное значение имеет приоритет, но разрешена рекурсия
                   * Идем глубже.
                   */
                  else if (typeof hash[i] == "object" && typeof hashExtender[i] == "object" && cfg.rec)
                     hash[i] = core.merge(hash[i], hashExtender[i], cfg);
               }
            }
         }
         else if(!(hashExtender === null && cfg.noOverrideByNull) && !cfg.preferSource)
            hash = hashExtender;

         return hash;
      },
      /**
       * Создание класса-наследника.
       *
       * @description
       * Класс наследник описывается объектом.
       * В нем поддерживаются следующие элементы
       *  - {Function} $constructor - Функция-конструктор нового класса.
       *  - {Object} $protected - объект-описание защищенных (protected) полей класса.
       *             Описывается как _ИмяПеременной : ЗначениеПоУмолчанию
       *  - {Function} любойМетод - методы класса
       * Также функция extend доступна как член конструктора любого класса, полученного с момощью нее
       * В этом случае первый аргумент не используется. Пример:
       * <pre>
       *    // Наследуем SomeClass от Abstract
       *    SomeClass = Abstract.extend({
       *       $protected: {
       *          _myProtectedVar: 10,
       *          _myProtectedArray: []
       *       },
       *       $constructor: function(cfg){
       *          this._doInit(); // Call method
       *       },
       *       _doInit: function(){
       *          // do something
       *       },
       *       doSomethingPublic: function(){
       *          // do something public
       *       }
       *    });
       * </pre>
       *
       * @param {Function|Object} classPrototype Родительский класс
       * @param {Object} classExtender Объект-описание нового класса-наследника
       * @returns {Function} Новый класс-наследник
       */
      extend : function(classPrototype, classExtender){
         var extProtected = classExtender.$protected,
            objectToString = Object.prototype.toString;
         if (!extProtected)
            extProtected = classExtender.$protected = {};

         var protoProtected = classPrototype.prototype && classPrototype.prototype.$protected;
         if (!('_needOptionsOverrideData' in extProtected) &&
            protoProtected && ('_needOptionsOverrideData' in protoProtected))
         {
            extProtected._needOptionsOverrideData = protoProtected._needOptionsOverrideData;
         }

         function mergeOptions(_protected, config){

            function mergeBlock(name, isNullableBlock) {
               if(_protected && name in _protected){
                  for(var i in _protected[name]){
                     if(_protected[name].hasOwnProperty(i) && i in config){
                        var cItem = _protected[name][i];
                        this._options[i] = core.merge(cItem instanceof Array ? [] : {}, cItem, { clone: true }); // Full copy
                        this._options[i] = core.merge(this._options[i], config[i], { noOverrideByNull: !isNullableBlock }); // Merge
                        delete config[i];
                     }
                  }
               }
            }

            mergeBlock.call(this, '_options', false);
            mergeBlock.call(this, '_nullable', true);
         }

         function getOverridenOptions(_protected, config) {
            function isObject(v) {
               return (typeof(v) === 'object') && (objectToString.call(v) === '[object Object]');
            }

            function overrideAll(result, srcRoot, name) {
               result[name] = srcRoot;

               if (isObject(srcRoot)) {
                  for (var i in srcRoot) {
                     if (srcRoot.hasOwnProperty(i)) {
                        result = overrideAll(result, srcRoot[i], name + '.' + i);
                     }
                  }
               }
               return result;
            }
            function getOverriden(root, nullableRoot, configRoot, result, prefix) {
               var k, v, srcRootV, isNullable, srcRoot;
               for (k in configRoot) {
                  if (configRoot.hasOwnProperty(k)) {
                     isNullable = nullableRoot && (k in nullableRoot) && !(k in root);
                     srcRoot = isNullable ? nullableRoot : root;

                     v = configRoot[k];
                     if (v !== null) {
                        if (k in srcRoot) {
                           srcRootV = srcRoot[k];
                           if (result === null) result = {};

                           if (isNullable) {
                              result = overrideAll(result, srcRootV, prefix + k);
                           } else if (isObject(v)) {
                              result[prefix + k] = srcRootV;
                              if (isObject(srcRootV))
                                 result = getOverriden(srcRootV, null, v, result, prefix + k + '.');
                           } else {
                              result = overrideAll(result, srcRootV, prefix + k);
                           }
                        }
                     } else if (isNullable) {
                        if (result === null) result = {};
                        result = overrideAll(result, nullableRoot[k], prefix + k);
                     }
                  }
               }

               return result;
            }

            return _protected && _protected._options &&
               getOverriden(_protected._options, _protected._nullable, config, null, '');
         }

         classExtender.$constructor = (function(_classPrototype, _classExtender){
            var
               constructors = {
                  parent : _classPrototype.prototype ? _classPrototype.prototype.$constructor : null,
                  child : _classExtender.$constructor
               }, $protected = _classExtender.$protected;

            function parseMarkup(cfg){
               var ctn = cfg;
               cfg = JSON.parse(cfg.getAttribute("config") || "{}");
               cfg.container = ctn;
               cfg.name = ctn.getAttribute("name") || cfg.name;

               function parseElem(elem){

                  if (elem instanceof Text){
                     return /\S/.test(elem.textContent) ? {type : "string", value : elem.textContent} : false;
                  }
                  else if (elem.namespaceURI == "ws-component"){
                     return {type : "string", value : "{component}"};
                  }
                  else if (elem.attributes.length){
                     var obj = {};
                     for (var atr in elem.attributes){
                        if (elem.attributes.hasOwnProperty(atr)){
                           var attr = elem.attributes[atr];
                           if (attr["name"]){
                              obj[attr["name"]] = attr["value"];
                           }
                        }
                     }
                     obj["content"] = elem.innerHTML;
                     return {type : "object", value : obj};
                  }
                  else{
                     var
                        o = {},
                        a = [],
                        lastType = null,
                        isArray = true;
                     for (var i = 0, l = elem.childNodes.length; i < l; i++){
                        var res = parseElem(elem.childNodes[i]);
                        if (res){
                           a.push(res);

                           isArray = lastType ? lastType === res["type"] : true;
                           lastType = res["type"];
                        }
                     }
                     if (isArray){
                        o["type"] = elem.localName;
                        o["value"]= [];
                        for (var aI = 0, aL = a.length; aI < aL; aI++){
                           o["value"].push(a[aI]["value"]);
                        }
                     }
                     return o;
                  }
               }


               var childNodes = cfg.container.childNodes;
               if (childNodes.length){
                  for (var i = 0, l = childNodes.length; i < l; i++){
                     var field = parseElem(childNodes[i]);
                     if (field){
                        cfg[field["type"]] = field["value"];
                     }
                  }
               }

               return cfg;
            }

            return function constructFn(cfg, needOptionsOverrideData){

               if (cfg instanceof Node){
                  cfg = parseMarkup(cfg);
               }

               needOptionsOverrideData = needOptionsOverrideData || $protected._needOptionsOverrideData;

               /**
                * Пропишем в this все из $protected без замещения и клонируя пустые ссылочные типы
                * К моменту запуска первого конструктора должны быть прописаны все члены $protected
                * фактически это замена объявления и установки дефолтных значений для protected членов класса
                * Установка идет по цепочке иерархии снизу вверх! Потому и без замещения.
                * Чтобы наследник мог перебить дефолтное значение родителя
                */
               core.merge(this, $protected, { clone: true, preferSource: true });
               /**
                * Новый тип опций - nullable
                * Допишем их начальные значения в _options
                * Если так получилось что _options отсутствовал а _nullable есть - сделаем _options
                */
               if($protected && $protected['_nullable']) {
                  if(!('_options' in this))
                     this._options = {};
                  core.merge(this._options, $protected['_nullable'], { clone: true, preferSource: true });
               }
               /**
                * Удалим _nullable из this, он там не нужен
                */
               delete this._nullable;

               /**
                * Обработаем опции конфигурации (если пришли)
                * Нужно взять все из текущего $protected и обработать, удалив из пришедших аргументов
                */
               if(cfg) {
                  if (needOptionsOverrideData) {
                     var newOverriden = getOverridenOptions($protected, cfg),
                        key, overriden;
                     if (newOverriden) {
                        overriden = this['_optionsOverriden'];
                        if (!overriden)
                           this['_optionsOverriden'] = newOverriden;
                        else {
                           for (key in newOverriden) {
                              if (newOverriden.hasOwnProperty(key))
                                 overriden[key] = newOverriden[key];
                           }
                        }
                     }
                  }
                  mergeOptions.call(this, $protected, cfg);
               }

               var res = constructors.child.call(this, cfg);

               if ('_allowEvents' in this){
                  this._allowEvents();
               }

               return res !== undefined ? res : this;
            };
         })(classPrototype, classExtender);

         var
            constructor = function(){},
            classResult = classExtender.$constructor;
         constructor.prototype = classPrototype.prototype;
         classResult.prototype = new constructor();
         for (var i in classExtender){
            if (classExtender.hasOwnProperty(i) && i != '$constructor')
               classResult.prototype[i] = classExtender[i];
         }
         // fix for the best browser in the world (IE) (c) ErshovKA
         // IE 7-8 не видит при переборе через for-in свойства valueOf и toString
         if('\v' == 'v') {
            var props = [ 'valueOf', 'toString' ];
            for(i = 0; i < props.length; i++) {
               if(classExtender.hasOwnProperty(props[i])) // property is redefined
                  classResult.prototype[props[i]] = classExtender[props[i]]
            }
         }

         classResult.prototype.$constructor = classExtender.$constructor;
         classResult.extend = function(_classExtender){
            return core.extend(this, _classExtender);
         };
         classResult.extendPlugin = function(pluginConfig){
            return core.extendPlugin(this, pluginConfig);
         };
         classResult.superclass = classPrototype.prototype;
         classResult.super = classResult.superclass ? classResult.superclass["$constructor"] : null;
         return classResult;
      },

      bootup : function(d, c){
         require(d, function(){
            var components = {};
            for (var i = 0, l = d.length; i < l; i++){
               var
                  liveCollection = document.getElementsByTagNameNS("ws-component", d[i].split("!")[1]),
                  deadCollection = [];

               for (var le = 0, leL = liveCollection.length; le < leL; le++){
                  deadCollection.push(liveCollection[le]);
               }

               for (var de = 0, deL = deadCollection.length; de < deL; de++){
                  var
                     comp = new (arguments[i])(deadCollection[de]),
                     name = comp.getName();
                  if (name){
                     components[name] = comp;
                  }
               }
            }
            if (c.length){
               require(c, function(){
                  for (var i = 0, l = arguments.length; i < l; i++){
                     arguments[i](components);
                  }
               });
            }
         });

      }
   };
   return core;
});