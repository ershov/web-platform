define("js!DataBoundControl", ["js!BaseControl"], function (BaseControl) {
   var DataBoundControl = BaseControl.extend({
      $protected : {
         _options : {
            value : ""
         }
      },
      $constructor : function(){
         DataBoundControl.super.apply(this, arguments);
      },
      val : function(v){
         if (v){
            this._context.setValue(this.getName(), this._options.value = v);
            this._setValue(this._options.value);
         }
         else{
            this._options.value = this._getValue();
         }

         return this._options.value;
      },
      _getValue : function(){

      },
      _setValue : function(val){

      },
      _onContextValueReceived : function(val){
         this._setValue(val);
      },
      _updateContextValue : function(val){
         this._context.setValue(this.getName(), val, false, true);
      }
   });

   return DataBoundControl;
});