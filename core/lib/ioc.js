define("js!ioc", [], function(){

   /**
    * IoC контейнер
    * Эта штука позволяет нам конфигурировать, какая конкретная реализация соответствует заданному интерфейсу
    * Все как во взрослых языках, ога.
    * Это используется например для:
    *    - конфигурирования какой транспорт использовать
    *    - конфигурирования system-wide логгера
    *
    * <pre>
    *    core.ioc.bind('ITransport', 'XHRPostTransport');
    *    core.ioc.bindSingle('ILogger', 'ConsoleLogger', { ...config...});
    *    ...
    *    core.ioc.resolve('ITransport', config);
    *    core.ioc.resolve('ILogger');
    * </pre>
    *
    * @class ioc
    * @singleton
    */
   return function(){
      var
         map = {},
         singletons = {};
      return {
         /**
          * @lends ioc.prototype
          */
         /**
          * Привязывает реализацию к интерфейсу.
          *
          * @param {String} interfaceName
          * @param {String} implementationName
          */
         bind: function(interfaceName, implementationName){
            map[interfaceName] = {
               implementation: implementationName,
               isSingle: 0
            };
         },
         /**
          * Привязывает единственный экземпляр реализации к указанному "интерфейсу"
          *
          * @param {String} interfaceName
          * @param {String} implementationName
          * @param {Object} [config]
          */
         bindSingle: function(interfaceName, implementationName, config) {
            map[interfaceName] = {
               implementation: implementationName,
               isSingle: 1,
               config: config || {}
            };
            singletons[interfaceName] = '';
         },
         /**
          * @param {String} interfaceName
          * @param {Object} [config]
          * @returns {Object}
          * @throws TypeError
          * @throws ReferenceError
          */
         resolve: function(interfaceName, config){
            if(interfaceName in map){
               var
                  binding = map[interfaceName],
                  classConstructorName = binding.implementation,
                  isSingleton = binding.isSingle;
               if(isSingleton && singletons[interfaceName])
                  return singletons[interfaceName];

               if(require.defined("classConstructorName") && typeof require(classConstructorName) == 'function') {
                  if(isSingleton)
                     return singletons[interfaceName] = new (require(classConstructorName))(binding.config);
                  else
                     return new new (require(classConstructorName))(config);
               }
               else
                  throw new TypeError("Implementation " + classConstructorName + " is not defined");
            }
            else
               throw new ReferenceError("No mappings defined for " + interfaceName);
         }
      };
   };
});
