define("js!BaseControl", ["core", "js!Abstract", "js!ControlStorage", "js!Context"], function(core, Abstract, ControlStorage, context){

   var BaseControl = Abstract.extend({
      $protected : {
         _id : null,
         _dotTplFn : null,
         _container : null,
         _context : null,
         _options : {
            name : ""
         }
      },
      $constructor : function(cfg){
         BaseControl.super.call(this, cfg);

         this._container  = cfg.container;
         this._context    = cfg.context || context.GlobalContext;

         if (this._dotTplFn){
            this._createMarkup();
         }
         this._container.setAttribute("id", this._id = this._generateId());

         this._contextUpdateHandlerFunction = $.proxy(this._contextUpdateHandler, this);

         this.setContext(this._context);

         ControlStorage.store(this);
      },
      getContainer : function(){
         return this._container;
      },
      getContext : function(){
         return this._context;
      },
      setContext : function(ctx){
         if (this._context && this._context instanceof context.Context){
            this._context.unsubscribe('onDataBind', this._contextUpdateHandlerFunction);
            this._context.unsubscribe('onFieldChange', this._contextUpdateHandlerFunction);
         }
         this._context = ctx;
         // Обновляем свое значение когда в контексте поменялись данные.
         if(this._context && this._context instanceof context.Context) {
            this._context.subscribe('onDataBind', this._contextUpdateHandlerFunction);
            this._context.subscribe('onFieldChange', this._contextUpdateHandlerFunction);
         }
         this._contextUpdateHandler();
      },
      getName : function(){
         return this._options.name;
      },
      getId : function(){
         return this._id;
      },
      /**
       * Обработчик смены значения в контексте
       * @param {Object} [event] Событие
       * @param {String} [field] Название изменившегося поля
       * @param {*} [value] Новое значение в контексте
       * @private
       */
      _contextUpdateHandler: function(event, field, value) {
         var name = this._options.name;
         if(name !== '' && this._context) {
            // Если нам пришел field - значит это onFieldChange
            if(field) {
               if(field == name) // Если это наше поле - обновимся
                  this._onContextValueReceived(value);
            }
            else // Иначе обычная схема
               this._onContextValueReceived(this._context.getValue(name));
         }
      },
      _onContextValueReceived : function(){

      },
      _generateId : function(){
         return Math.random().toString(36).substring(7);
      },
      _createMarkup : function(){
         if (!this._container || !/ws-has-markup/.test(this._container.className)){
            var
               e,
               container = document.createElement("div"),
               parentNode = this._container ? this._container.parentNode : undefined;

            container.innerHTML = this._dotTplFn(this._options);
            e = container.childNodes[0];

            if (parentNode){
               parentNode.replaceChild(e, this._container);
            }

            this._container = e;
            this._container.className = this._container.className + " ws-has-markup";
            this._container.setAttribute("data-component", this._container.localName);
         }
      },
      _removeContainer : function(){
         var
            parent = this._container.parentNode;

         parent.removeChild(this._container);
      },
      destroy : function(){
         this._removeContainer();
         ControlStorage.remove(this);

         this._context = null;

         BaseControl.superclass.destroy.apply(this, arguments);
      }
   });

   return BaseControl;
});