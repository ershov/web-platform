define("js!Abstract", ["core"], function(core){

   /**
    * Объект события, приходит в обработчики после {@link Abstract#_notify}
    * @class
    * @name EventObject
    */
   var EventObject = function(eventName) { this._eventName = eventName; };
   EventObject.prototype = {
      /**
       * @lends EventObject.prototype
       */

      _isBubbling: true,
      _result: undefined,
      _eventName: null,

      /**
       * Отменить дальнейшую обработку
       */
      cancelBubble: function(){ this._isBubbling = false; },

      /**
       * Будет ли продолжена дальнейшая обработка
       * @returns {Boolean}
       */
      isBubbling: function() { return this._isBubbling; },

      /**
       * Возвращает результат
       * @returns {*}
       */
      getResult: function(){ return this._result; },

      /**
       * Устанавливает результат
       * @param {*} r
       */
      setResult: function(r){ this._result = r; }
   };


   /**
    * Абстрактный класс.
    * Здесь живет событийная модель.
    * Все, кому нужны события, должны наследоваться от него
    *
    * EventObject
    * В обработчик события первым параметром ВСЕГДА приходит объект, используя который можно рабортать с текущим событием.
    * Событию можно запретить всплытие(Bubbling) и сменить результат.
    * В зависимости от результата оно продолжит выполнятся дальше, либо перестанет.
    * Интерфейс EventObject:
    * void     cancelBubble()    - метод запрещающий дальнейшее всплытие
    * boolean  isBubbling()      - получение статуса всплытия
    * boolean  getResult()       - получение результата события
    * void     setResult(result) - смена результата события
    *
    * @class Abstract
    * @cfg {object} handlers Хэндлеры событий
    * Например:
    * <pre>
    *    {
    *       onInit: function(){},
    *       onSome: function(){}
    *    }
    * </pre>
    */
   var Abstract = core.extend({}, {
      /**
       * @lends Abstract.prototype
       */
      /**
       * @event onInit Событие, возникающее непосредственно после построения экземпляра класса через attachInstance
       * @param {Object} eventObject описание в классе Abstract
       */
      /**
       * @event onReady Какое-то событие, пока не ясно какое
       * @param {Object} eventObject описание в классе Abstract
       */
      /**
       * @event onDestroy Возникает при уничтожении
       * @param {Object} eventObject описание в классе Abstract
       */
      $protected: {
         _eventQueue: [],
         _eventsAllowed: false,
         _isDestroyed : false,
         _handlers : {},
         _events : {}
      },
      $constructor : function(cfg){
         if (cfg !== undefined && typeof cfg.handlers == "object")
            this._handlers = cfg.handlers;
         this._publish('onInit', 'onReady', 'onDestroy');
      },
      init: function() {
         this._notify('onInit');
      },
      /**
       * "Описывает" класс. Удобно для логгирования ошибок.
       * Дочерние классы могу переопределять для более детального описания
       * @returns {String} "Описание" класса
       */
      describe: function() {
         return 'Abstract';
      },
      /**
       * Декларирует наличие у объекта событий
       * События могут быть переданы в виде строки, в виде массива строк.
       */
      _publish : function(/*$event*/){
         var events = core.unpack(Array.prototype.slice.call(arguments));
         for (var i = 0, li = events.length; i < li; i++){
            var event = events[i];
            this._events[event] = this._events[event] || [];
            if (this._handlers[event] !== undefined){
               this.subscribe(event, this._handlers[event]);
               delete this._handlers[event];
            }
         }
      },
      /**
       * Извещает всех подписантов события
       * Все аргументы после имени события будут переданы подписантам
       * @param {string} event Имя события
       * @param [arg1, [...]] Параметры, получаемые подписантами
       * @returns {Boolean|String|Object} Результат выполнения цепочки
       */
      _notify : function(event/*, payload*/){

         var payload, eventState, result = undefined;

         if(!this._eventsAllowed) {
            payload = Array.prototype.slice.call(arguments);
            this._eventQueue.push(payload);
         }
         else if (this._events){
            var handlers = this._events[event],
               ln;
            if (handlers) {
               ln = handlers.length;
               if (ln) {
                  eventState = new EventObject(event);
                  payload = Array.prototype.slice.call(arguments);
                  payload[0] = eventState;

                  for(var i = 0; i < ln; i++) {
                     try {
                        var e = handlers[i];
                        if (e)
                           e.apply(this, payload);

                        if(!eventState.isBubbling() || !this._events)
                           break;
                     } catch(e) {
                        core.ioc.resolve('ILogger').error(
                           this.describe(),
                           "Event handler for '" + event + "' returned error: " + e.message,
                           e);
                     }
                  }
                  result = eventState.getResult();
               }
            }
            else{
               core.ioc.resolve('ILogger').error(
                  "Abstract",
                  "Event '" + event + "' have not published yet");
            }
         }
         return result;
      },

      _allowEvents: function(){
         if(this._eventsAllowed === false) {
            this._eventsAllowed = true;
            for(var i = 0, l = this._eventQueue.length; i < l; i++)
               this._notify.apply(this, this._eventQueue[i]);
            this._eventQueue = [];
         }
      },
      once : function(event, handler) {
         this.subscribe(event, function() {
            handler.apply(this, arguments);
            this.unsubscribe(event, arguments.callee);
         });
      },
      /**
       * Подписиывает делегата на указанное событие текущего объекта
       * @param {string} event Событие
       * @param {Function} $handler Функция-делегат
       * @throws Error Выкидывает исключение при отсутствии события и передаче делегата не-функции
       */
      subscribe : function(event, $handler){
         if (this._events[event] === undefined)
            throw new Error("Event '" + event + "' is not registered");
         else{
            var handler = core.unpack($handler, ["object", "array"]);
            for (var i = 0, li = handler.length; i < li; i++){
               if (handler[i]) {
                  if (typeof handler[i] == 'function')
                     this._events[event].push(handler[i]);
                  else
                     throw new TypeError("Event '" + event + "' has unexpected handler");
               }
            }
         }
         return this;
      },
      /**
       * Снять подписку заданного колбэка с заданного события
       * @param {String} event Событие
       * @param {Function} handler Колбэк
       * @return {Abstract}
       */
      unsubscribe: function(event, handler) {
         var handlers = this._events[event];
         if (handlers) {
            var newHandlers, i, last, ln;
            for (i = 0, ln = handlers.length; i !== ln; i++) {
               if (handlers[i] === handler) {
                  newHandlers = handlers.slice(0, i);

                  i++;
                  last = i;
                  for (; i !== ln; i++) {
                     if (handlers[i] === handler) {
                        if (last !== i)
                           newHandlers = newHandlers.concat(handlers.slice(last, i));
                        last = i + 1;
                     }
                  }

                  if (last !== ln) {
                     newHandlers = newHandlers.concat(handlers.slice(last, ln));
                  }

                  this._events[event] = newHandlers;
                  break;
               }
            }
         }
         return this;
      },
      /**
       * Снимает все обработчики с события
       * @param {string} event Имя события
       */
      unbind: function(event) {
         this._events[event] = [];
         return this;
      },
      destroy: function() {
         this._notify('onDestroy');
         this._events = null;
         this._handlers = null;
         this._isDestroyed = true;
      },
      hasEvent : function(name){
         return this._events && !!this._events[name];
      },
      hasHandlersForEvent : function(name){
         return this.hasEvent(name) && (this._events[name].length > 0);
      },

      _isOptionDefault: function(option) {
         return !(('_optionsOverriden' in this) && (option in this['_optionsOverriden']));
      },

      _getOverridenOptionDefaultValue: function(option) {
         return this._isOptionDefault(option) ? undefined : this['_optionsOverriden'][option];
      },

      _hasOverridenOptions: function() { return !!this['_optionsOverriden']; }
   });

   return Abstract;
});