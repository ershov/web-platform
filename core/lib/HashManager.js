define("js!HashManager", ["core", "js!EventBus"], function(core, EventBus){

   return new (core.extend({}, {
      $protected : {
         _val : null
      },
      $constructor : function(){
         if (window){
            var self = this;
            if (navigator.userAgent.match(/MSIE 7/)){//IE7 detecting
               var oldHash = decodeURI(window.location.hash);
               setInterval(function(){
                  if (oldHash !== decodeURI(window.location.hash)){
                     oldHash = decodeURI(window.location.hash);
                     self._trigger();

                  }
               },100);
            }
            else{
               window.onhashchange = function(){
                  self._trigger();
               }
            }
            self._trigger();
         }
      },
      _trigger : function(){
         var
            hash = window.location.hash,
            paths = hash.replace("#", "").split("/");

         this._val = {
            type : paths[0],
            target : paths[1]
         };

         EventBus.channel("hash").trigger(this._val["type"], this._val["target"]);
      },
      get : function(){
         return this._val;
      },
      on : function(type, fn, scope){
         EventBus.channel("hash").on(type, fn, scope);
      },
      off : function(type, fn, scope){
         EventBus.channel("hash").off(type, fn, scope);
      },
      has : function(type, fn, scope){
         EventBus.channel("hash").has(type, fn, scope);
      }
   }))();
});