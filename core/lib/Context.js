define("js!Context", ["core", "js!Abstract", "js!Enum"], function(core, Abstract, Enum){

   /**
    * Контекст области.
    * Отвечает за управление данными в объекте.
    * Здесь логика наследования контекстов и проброса методов.
    *
    * @class Context
    * @cfg {Boolean} isGlobal Является ли данный контекст глобальным.
    * @extends Abstract
    */
   var Context = Abstract.extend({
      /**
       * @lends Context.prototype
       */
      /**
       * @event onDataBind Событие, происходящее в момент изменения контекста
       * @param {Object} eventObject описание в классе Abstract
       */
      /**
       * @event onFieldChange Событие, происходящее при смене значения поля текущего или вышестоящего контекста
       * @param {Object} eventObject описание в классе Abstract
       * @param {String} fieldName Имя измененного поля
       * @param {*} value Значение поля
       */
      $protected: {
         _options: {
            isGlobal: false
         },
         _previousContext: null,
         _context: {},
         _parentContextUpdateHandler: null,
         _parentContextFieldUpdateHandler: null
      },
      $constructor : function(){
         Context.super.apply(this, arguments);

         var self = this;

         this._parentContextUpdateHandler = function() {
            self._notify('onDataBind');
         };

         this._parentContextFieldUpdateHandler = function(event, field, value) {
            if(self._context.get(field) === undefined) // если у нас самих нет такого значения ...
               self._notify('onFieldChange', field, value); // ... известим ниже о смене выше
         };

         if(this._options.isGlobal === false)
            this._previousContext = GlobalContext;

         this._subscribeOnParentUpdates();

         this._context = new ContextObject();
         this._publish('onDataBind', 'onFieldChange');
      },
      _subscribeOnParentUpdates: function() {
         if(this._previousContext) {
            this._previousContext.subscribe('onDataBind', this._parentContextUpdateHandler);
            this._previousContext.subscribe('onFieldChange', this._parentContextFieldUpdateHandler);
         }
      },
      _unsubscribeOnParentUpdates: function() {
         if(this._previousContext) {
            this._previousContext.unsubscribe('onDataBind', this._parentContextUpdateHandler);
            this._previousContext.unsubscribe('onFieldChange', this._parentContextFieldUpdateHandler);
         }
      },
      /**
       * Устанавливает предыдущий контекст
       * @param {Context} previous Контекст который необходимо привязать предыдущим к текущему
       * @return {Context}  текущий контекст
       */
      setPrevious: function(previous){
         if(previous !== null && previous instanceof Context) {
            this._unsubscribeOnParentUpdates();
            this._previousContext = previous;
            this._subscribeOnParentUpdates();
         }
         return this;
      },
      /**
       * Получение предыдущего контекста.
       * @return {Context} предыдущий контекст или null если он отсутствует
       */
      getPrevious: function(){
         return this._previousContext;
      },
      /**
       * Метод, позволяющий изменить данные, хранимые контекстом.
       * @param {Object|Record} context новый контекст
       */
      setContextData: function(context){
         this._context = new ContextObject({objectData : context});
         this._notify('onDataBind');
      },
      /**
       * Проверка контекста на то является он глобальным или нет
       * @return {Boolean} флаг глобальности
       */
      isGlobal : function(){
         return this._options.isGlobal;
      },
      /**
       * проверяет пуст ли контекст
       * @return {Boolean}
       */
      isEmpty : function(){
         return this._context.isEmpty();
      },
      /**
       * Получить запись по которой построен контекст. Если контекст построен без записи вернет null
       * @returns {Record} если в контекст положили запись, то возвращаем ее
       */
      getRecord : function(){
         return this._context.getRecord();
      },
      /**
       * Метод получения значения поля из контекста. Если поле не найдено в текущем контексте, то ищется в предыущем. и так , пока не найдется.
       * @param {string} fieldName имя поля, значение которого необходимо вернуть
       * @param {Function} [func] функция валидации значения, принимает один параметр - значение поля в контексте,
       * если возвращает TRUE, то значение ищется в предыдущем контексте, иначе должен возвращать значение поля
       * @returns {string}
       */
      getValue : function(fieldName, func){
         var retval = this._context.get(fieldName);
         return (typeof func === 'function' ? func(retval) : retval === undefined) ? this._getValueUp(fieldName, func) : retval;
      },
      /**
       * Получает значение поля из контекста. Отличается от getValue тем, что не ищет в "родительских" контекстах
       * @param {String} fieldName Название поля
       * @returns {*}
       */
      getValueSelf: function(fieldName){
         return this._context.get(fieldName);
      },
      /**
       * Получение оригинального контекста в котором непосредственно расположено поля.
       * Если такого поля во всех родительских контекстах нет - вернет undefined.
       *
       * @param {String} fieldName имя поля
       * @return {Context || undefined} объект контекста или undefined
       */
      getFieldOrigin: function (fieldName){
         var result = this._context.get(fieldName);
         if(result === undefined && this._previousContext)
            result = this._previousContext.getFieldOrigin(fieldName);
         else if(result !== undefined)
            result = this;
         return result;
      },
      /**
       * Получение значения из родительского контекста
       * @param  fieldName имя поля, значение которого необходимо вернуть
       * @param {Function} [func] функция валидации значения, принимает один параметр - значение поля в контексте,
       * если возвращает TRUE, то значение ищется в предыдущем контексте
       * @return {*} Значение из контекста или undefined
       */
      _getValueUp: function(fieldName, func){
         if(this._previousContext !== null && this._previousContext instanceof Context)
            return this._previousContext.getValue(fieldName, func);
         else
            return undefined;
      },
      /**
       * Установка значения в контекст
       * @param {String} fieldName имя поля, в которое будет установлено значение
       * @param {*} value значение, которое будет установлено
       * @param {Boolean} [toSelf] флаг устанавливать значение только в себя или можно выше
       * @param {Boolean} [noChangeNotify] флаг, управляющий извещением об изменении поля
       */
      setValue : function(fieldName, value, toSelf, noChangeNotify){
         var self = this;
         function setV( field_name, val, to_self, no_change_notify ){
            // Выясним где ...
            var ctx = (!!to_self) ? self : (self.getFieldOrigin(field_name) || self);
            // и обновим значение
            ctx['_updateValue'](field_name, val, no_change_notify);
         }
         // А вдруг кто-то передал объект
         if( typeof( fieldName ) == 'object' ){
            for(var key in fieldName)
               setV( key, fieldName[key], value, toSelf ); // Смещаем параметры
         }
         else
            setV( fieldName, value, toSelf, noChangeNotify );
      },
      _updateValue: function(fieldName, value, noChangeNotify) {
         var
            currentValue = this._context.get(fieldName),
            isEquals;

         if(currentValue && currentValue.equals)
            isEquals = currentValue.equals(value);
         else
            isEquals = (currentValue === value);

         if(!isEquals) { // Если значение отличается ...
            this._context.set(fieldName, value); // ... обновим и ...
            if(noChangeNotify !== true) // ... если извещаем ...
               this._notify('onFieldChange', fieldName, value); // ... известим
         }
      },
      /**
       * Удаление значения из контекста
       * @param {string} fieldName имя поля, из которого удалять
       * @param {Boolean} [toSelf]  если false и не удалил в текущем, пытается удалить в предыдущем.
       */
      removeValue : function(fieldName, toSelf){
         var result = this._context.remove(fieldName);
         if(!result && !toSelf && this._previousContext !== null && this._previousContext instanceof Context)
            return this._previousContext.removeValue(fieldName, toSelf);
         else
            return result;
      },
      /**
       * Установка значения в себя, без проброса в родительский контекст
       * @param {string} fieldName имя поля, в которое будет установлено значение
       * @param value значение, которое будет установлено
       * @param {Boolean} [noChangeNotify] флаг отключения извещения об изменениях
       */
      setValueSelf : function(fieldName, value, noChangeNotify) {
         this.setValue(fieldName, value, true, noChangeNotify);
      },
      /**
       * Вставляет в контекст объект как связанный
       * @param {Record || Object} values значения для вставки
       * @param {String} link имя связи
       */
      insert : function(values, link){
         this._multiOperation(values, link, 'insert');
      },
      /**
       * Удаляет из контекста объект по связи
       * @param {Record || Object} values значения для вставки
       * @param {String} link имя связи
       */
      remove : function(values, link){
         this._multiOperation(values, link, 'remove');
      },
      /**
       * Метод работы со связанными объектами
       * @param {Record || Object} values значения для вставки
       * @param {String} link имя связи
       * @param {String} type  Тип действия 'insert' || 'remove'
       */
      _multiOperation : function(values, link, type){
         link = typeof link === 'string' ? link + '.' : '';
         /*if (values instanceof Record){ TODO : прилепить record'ы
            values = values.toObject();
         }*/
         if(values instanceof Object){
            for (var i in values){
               if(values.hasOwnProperty(i)) {
                  if(type == 'remove')
                     this.removeValue(link + i, true);
                  if(type == 'insert')
                     this.setValueSelf(link + i, values[i]);
               }
            }
         } else if(values === false || values === null || values === undefined){
            // Вычищение связанных значений из контекста с попыткой понять, что же вычищать
            var ctx = this._context.getRecord() || this._context.toObject();
            for(var k in ctx){
               if(ctx.hasOwnProperty(k) && k.indexOf(link) === 0)
                  this.removeValue(k, true);
            }
         }
         this._notify('onDataBind');
      },
      destroy: function() {
         this._unsubscribeOnParentUpdates();
      },
      toObject: function(recursive) {
         var result = (this._context && this._context.toObject()) || {};

         if(recursive) {
            var parent = this.getPrevious();
            if(parent) {
               var parentObj = parent.toObject(true);
               for(var i in parentObj) {
                  if(parentObj.hasOwnProperty(i) && !(i in result)) {
                     result[i] = parentObj[i];
                  }
               }
            }
         }

         return result;
      }
   });

   var ContextObject = core.extend({}, {
      /**
       * @lends ContextObject.prototype
       */
      $protected: {
         _options: {
            objectData: false
         },
         _record: null,
         _isEmpty: true,
         _contextObject: {}
      },
      $constructor : function(){
         this._contextObject = {};
         if(this._options.objectData !== false){
            if(typeof(this._options.objectData) !== 'object')
               return;
            var isRecord = ('get' in this._options.objectData && 'hasColumn' in this._options.objectData),
               object = isRecord ? this._options.objectData.toObject() : this._options.objectData;

            if(isRecord)
               this._record = this._options.objectData;

            for(var i in object){//перелопачиваем объект переданный в конфигурации в свой формат
               if(object.hasOwnProperty(i)) {
                  this._contextObject[i] = {
                     value : object[i],
                     isRecord : isRecord
                  };
                  this._isEmpty = false;
               }
            }
         }
      },
      /**
       * Проверка на пустоту
       * @return {Boolean} пуст контекст или нет.
       */
      isEmpty : function(){
         return this._isEmpty;
      },
      /**
       * Получение целостного рекорда из контекста. Если контекст был построен не по рекорду вернет null
       * @return {Record} Запись по которой был построен или null при отсутствии
       */
      getRecord : function(){
         return this._record;
      },
      toObject: function() {
         var r = {};
         for(var f in this._contextObject) {
            if(this._contextObject.hasOwnProperty(f))
               r[f] = this._contextObject[f].value
         }
         return r;
      },
      /**
       * Меняет значение поля
       * @param {String} fieldName имя поля
       * @param value значение поля
       */
      set : function(fieldName, value){
         var field = this._contextObject[fieldName],
            isRecord = field ? field['isRecord'] : false;
         this._contextObject[fieldName] = {
            value : value,
            isRecord : isRecord
         };
         if(isRecord)   //Если в контексте рекорд с таким полем, то его тоже сетим
            this._record.set(fieldName, value);
         this._isEmpty = false;
      },
      /**
       * Получение значения поля
       * @param {String} fieldName имя поля
       * @return value значение поля или undefined при его отсутствии
       */
      get : function(fieldName){
         return this._contextObject[fieldName] !== undefined ? this._contextObject[fieldName].value : undefined;
      },
      /**
       * Удаляет поле из объекта контекста.
       * @param {String} fieldName имя поле которое необходимо удалить.
       * @return {Boolean} result результат удаления. произошло оно или нет.
       */
      remove : function(fieldName){
         var result = false,
            isRecord, value;
         if(this._contextObject[fieldName]){
            result = true;
            isRecord = this._contextObject[fieldName]['isRecord'];
            value = isRecord ? this.get(fieldName) : this._contextObject[fieldName].value;
            if(isRecord) {
               if(value instanceof Enum)
                  value.set(null);
               else
                  this.set(fieldName, null);
            } else
               this._contextObject[fieldName].value = undefined;
         }
         return result;
      }
   });


   var GlobalContext = new Context({ isGlobal: true });

   return {
      Context : Context,
      GlobalContext : GlobalContext
   };
});