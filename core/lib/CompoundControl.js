define("js!CompoundControl", ["core", "js!BaseControl"], function(core, BaseControl){

   var CompoundControl = BaseControl.extend({
      $protected : {
         _components: {}
      },
      $constructor : function(cfg){
         CompoundControl.super.call(this, cfg);

         var innerContainers = this._getInnerContainers(this._container);
         for(var i = 0, l = innerContainers.length; i < l; i++){
            var component = this._attachChild(innerContainers[i]);
            component.setContext(this._context);
            this._components[component.getName()] = component;
         }
      },
      _attachChild : function(childElement){
         var componentName = /ws-has-markup/.test(childElement.className) ? childElement.getAttribute("data-component") : childElement.localName;
         return new (require(["js", componentName].join("!")))(childElement);
      },
      _getInnerContainers : function(root){
         var
            res = [],
            withMarkup = root.getElementsByClassName("ws-has-markup"),
            withoutMarkup = root.getElementsByTagNameNS("ws-component", "*"),
            p;
         for (var i = 0, l = withMarkup.length; i < l; i++){
            p = withMarkup[i].parentNode;
            while(p !== null || !/ws-has-markup/.test(p.className)){
               p = p.parentNode;
            }
            if (p && p === root){
               res.push(p);
            }
         }

         for(i = 0, l = withoutMarkup.length; i < l; i++){
            res.push(withoutMarkup[i]);
         }

         return res;
      },
      destroy : function(){
         for (var i in this._components){
            if (this._components.hasOwnProperty(i)){
               this._components[i].destroy();
            }
         }
         CompoundControl.superclass.destroy.apply(this, arguments);
      }
   });

   return CompoundControl;
});
