define("js!ControlStorage", ["js!Deferred"], function(Deferred){

   var ControlStorage = {
      /**
       * @lends ControlStorage.prototype
       */
      _storage : {},
      _waitingChildren: {},
      _waitingChildrenByName: {},
      /**
       * Сохраняет элемент управления в хранилище
       *
       * @param {BaseControl} control
       * @returns {String|Boolean} ID элемента, если сохранен, false в противном случае
       */
      store : function(control){
         var id = false;
         this._storage[id = control.getId()] = control;
         if(this._waitingChildren[id]) {
            try {
               this._waitingChildren[id].callback(control);
            }
            finally {
               delete this._waitingChildren[id]
            }
         }
         var name = control.getName();
         if(this._waitingChildrenByName[name] && !this._waitingChildrenByName[name].isReady()) {
            try {
               this._waitingChildrenByName[name].callback(control);
            }
            finally {
               delete this._waitingChildrenByName[name];
            }
         }
         return id;
      },
      /**
       * Убирает элемент управления из хранилища
       * @param {BaseControl} control
       */
      remove : function(control){
         var id = control.getId();
         var name = control.getName();
         if(id in this._storage) {
            this._storage[id] = null;
            delete this._storage[id];
         }
         if(id in this._waitingChildren) {
            this._waitingChildren[id] = null;
            delete this._waitingChildren[id];
         }
         if(name in this._waitingChildrenByName) {
            this._waitingChildrenByName[name] = null;
            delete this._waitingChildrenByName[name];
         }
      },
      /**
       * Получение контрола по идентификатору
       * @param {String}  id
       * @return {BaseControl} найденный контрол
       */
      get : function(id){
         if (this._storage[id] === undefined)
            throw new Error("ControlStorage : id = '" + id + "' not stored");
         return this._storage[id];
      },
      /**
       * Хранится ли контрол с таким идентификатором
       * @param {String} id   идентификатор контрола
       * @returns {Boolean}   нашолся ли контрол
       */
      contains : function(id){
         return this._storage[id] !== undefined;
      },
      /**
       * Получение контрола по имени
       * @param {String} name имя контрола
       * @param {Object} [classObject] класс, интстансом которого должен быть контрол
       * @return {BaseControl} найденный контрол
       */
      getByName: function(name, classObject) {
         for(var id in this._storage) {
            if(this._storage.hasOwnProperty(id)) {
               if(this._storage[id].getName() == name){
                  if(classObject && !(this._storage[id] instanceof classObject)){
                     continue;
                  }
                  return this._storage[id];
               }
            }
         }
         throw new Error("ControlStorage : control with name '" + name + "' is not stored");
      },
      /**
       * Хранится ли контрол с таким именем
       * @param {String} name   имя контрола
       * @returns {Boolean}   нашолся ли контрол
       */
      containsByName : function(name){
         for(var id in this._storage) {
            if(this._storage.hasOwnProperty(id)) {
               if(this._storage[id].getName() == name)
                  return true;
            }
         }
         return false;
      },
      /**
       * Ожидание контрола по идентификатору
       * @param {String} id идентификатор контрола
       * @return {Deferred}
       */
      waitChild: function(id){
         if(id in this._storage)
            return new Deferred().callback(this._storage[id]);
         else
            return (id in this._waitingChildren) ?
               this._waitingChildren[id] :
               (this._waitingChildren[id] = new Deferred());
      },
      /**
       * Ожидание контрола по имени
       * @param {String} name Имя контрола
       * @return {Deferred}
       */
      waitChildByName: function(name){
         if(this.containsByName(name))
            return new Deferred().callback(this.getByName(name));
         else
            return (name in this._waitingChildrenByName) ?
               this._waitingChildrenByName[name] :
               (this._waitingChildrenByName[name] = Deferred());
      },
      getControls: function(){
         return this._storage;
      }
   };

   return ControlStorage;
});