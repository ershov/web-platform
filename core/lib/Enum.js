define("js!Enum", ["core"], function (core) {

    var Enum = core.extend({}, {
       /**
        * @lends Enum.prototype
        */
       $protected:{
          _options:{
             availableValues: null
          },
          _nullable:{
             currentValue: undefined
          },
          _curValIndex : undefined,
          _availableValues : {},
          _hashCode: '',
          _fallbackVal: undefined,
          _initialValue: undefined
       },
       $constructor : function(){
          var
             avValues = this._options.availableValues,
             curValue = this._options.currentValue;
          curValue = (curValue === null || curValue === 'null') ? 'null' : parseInt(curValue, 10);
          if(avValues && typeof(avValues) == 'object'){
             var isFirst = true;
             for(var i in avValues){
                if(!avValues.hasOwnProperty(i))
                   continue;
                if(typeof(avValues[i]) !== 'function' && !(typeof(avValues[i]) == 'object' && avValues[i] !== null)){
                   var iKey = (i === null || i === 'null') ? 'null' : parseInt(i, 10);
                   if(isFirst) {
                      this._fallbackVal = iKey;
                      isFirst = false;
                   }
                   this._availableValues[i] = avValues[i];
                   this._curValIndex = (iKey === curValue) ? iKey : this._curValIndex;
                }
             }
          }else {
             throw new Error ('Class Enum. Option availableValues must be set to object');
          }
          if(this._curValIndex === undefined) {
             if(this._fallbackVal === undefined)
                throw new Error ('Class Enum. No values to build');
             else
                this._curValIndex = iKey;
          }
          this._initialValue = this._curValIndex;
       },
       valueOf: function(){
          return this.getCurrentValue();
       },
       /**
        * @return {*} "индекс" текущего значения перечисляемого
        */
       getCurrentValue: function() {
          return this._curValIndex == "null" ? null : this._curValIndex;
       },
       /**
        * @return {Object} хэш-мэп доступных значений для данного перечисляемого
        */
       getValues: function() {
          return this._availableValues;
       },
       /**
        * @deprecated Используйте toObject(). Будет удалено с версии 3.6.0
        */
       get : function(){
          return {
             'values': this._availableValues,
             'current': this._curValIndex
          }
       },
       /**
        * Установить текущее значение перечисляемого
        * @param index индекс нового текущего значения
        * @throws {Error} в случае, если указанный индекс отсутствует в текущем Enum'е
        */
       set: function(index){
          // null преобразовываем к строке 'null'
          index = (index === null) ? 'null' : index;
          if(index in this._availableValues) {
             this._hashCode = '';
             this._curValIndex = index;
          }
          else {
             // Попытались сбросить Enum, но null не допускается.
             if(index === 'null')
                this.set(this._initialValue);
             else // Что-то иное
                throw new Error('Class Enum. Unsupported index: ' + index);
          }
       },
       /**
        * Возвращает представление Enum в виде объекта.
        * Можно использовать для создания клона Enum
        * <pre>
        *    var enumClone = new $ws.proto.Enum(original.toObject());
        * </pre>
        * @returns {Object} Представление в виде объекта
        */
       toObject: function() {
          return {
             availableValues: this.getValues(),
             currentValue: this.getCurrentValue()
          }
       },
       hashCode: function() {
          if(this._hashCode === '') {
             this._hashCode = 17 + 31 * Object.keys(this._availableValues).length;
             for(var i in this._availableValues) {
                if(this._availableValues.hasOwnProperty(i) && i !== undefined) {
                   var v = parseInt(i, 10);
                   this._hashCode += 31 * (isNaN(v) ? -1 : v);
                }
             }
          }
          return this._hashCode;
       },
       /**
        * Проверяет данный объект на совпадение с другим.
        * Проверяется как такущее выставленное значение, так и набор допустимых
        *
        * @param obj Объект, с которым сравниваем
        * @return {Boolean} Совпадает или нет
        */
       equals: function(obj) {
          return obj instanceof Enum &&                  // this is an enum
             this.hashCode() == obj.hashCode() &&              // it stores same values
             this.getCurrentValue() == obj.getCurrentValue();  // current value is the same
       },
       rollback : function(val){
          this.set(val);
       },
       toString: function() {
          return "" + this._availableValues[this._curValIndex];
       },
       /**
        * Клонирует текущий объект
        * @return {Enum}
        */
       clone: function(){
          return new Enum({
             currentValue: this.getCurrentValue(),
             availableValues: this.getValues()
          });
       }
    });

   return Enum;
});