var pg = require('pg');
var conString = "postgres://postgres:postgres@localhost:5432/contacts";
var pgClient = new pg.Client(conString);

pgClient.connect();

module.exports = pgClient;
